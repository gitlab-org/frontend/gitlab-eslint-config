module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
    'prettier',
    'prettier/vue',
    'plugin:promise/recommended',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  plugins: ['babel', 'filenames', 'promise', 'import', '@gitlab/i18n', '@gitlab/vue-i18n'],
  rules: {
    'babel/camelcase': [
      'error',
      {
        properties: 'never',
        ignoreDestructuring: true,
      },
    ],
    camelcase: 'off',
    'filenames/match-regex': ['error', '^[a-z0-9_]+$'],
    'no-implicit-coercion': [
      'error',
      {
        boolean: true,
        number: true,
        string: true,
      },
    ],
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['acc', 'accumulator', 'el', 'element', 'state'],
      },
    ],
    'promise/catch-or-return': [
      'error',
      {
        allowFinally: true,
      },
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'any',
          normal: 'never',
          component: 'always',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/component-name-in-template-casing': 'off',
  },
};
